import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class Adressbuch {	
	
	private List<Kontakt> kontakte = null; // Hier ist eine List vieeeeel praktischer als "private Kontakt[] kontakte = null;"
	
	public void addKontakt(Kontakt kontakt) {
		kontakte.add(kontakt);
	}
	
	
	public static Adressbuch readFromFile(String filename) {
		Adressbuch adressbuch = new Adressbuch();
		
        BufferedReader bufferedReader = null;
        try {
        	bufferedReader = new BufferedReader(new FileReader(filename));
            
            String line;
            while (null != (line = bufferedReader.readLine())) {
                Kontakt kontakt = Kontakt.createFromRawString(line);
//            	Kontakt kontakt2 = new Kontakt(line);
            	
                if (kontakt != null) {
                	adressbuch.addKontakt(kontakt);
                }
            }
        } catch (FileNotFoundException e) {
        	System.err.println("Datei nicht gefunden!");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != bufferedReader) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return adressbuch;
	}
	
	public void saveToFile(String filename) {
		
		PrintWriter printWriter = null;
        try {
        	printWriter = new PrintWriter(new FileWriter(filename));
        	for (Kontakt k : this.kontakte) {
        		printWriter.println(k.toRawString());
        	}
        } catch (IOException e) {
        	System.err.println("Ein Fehler ist aufgetreten, bitte versuche es erneut");
            e.printStackTrace();
        } finally {
            if (null != printWriter) {
            	printWriter.close();
            }
        }
	}
	
	/**
	 * Standardkonstruktor
	 */
	public Adressbuch() {
		kontakte = new ArrayList<>(); // erzeuge leere Liste
	}

	/**
	 * Ausgabe der Kontakte
	 */
	public void show() {
		if (kontakte != null && !kontakte.isEmpty()) {
			System.out.println("-----------------/ " + "Daniel's Adressbuch" + " \\-----------------");
			boolean start = true;
			for (Kontakt k : kontakte) {
				if (true == start){
					start = false;
				} else {
					System.out.println("--------------------------------------");
				}
				k.show(); // "fertig"
			}
			System.out.println("----------------- Adressbuch Ende ------------------");
		}
	}
	
}
