
public class Main {
	
	public static void main(String[] args) {
		SaveMethod.chooseMethod(); //GUI öffnen
	}
	
	//Kontakt hinzufügen
	static void addEntries(Adressbuch adressbuch) {
		addKontakt(adressbuch, "Daniel", "Becker", "Hamburg", "19.03.2002", "unbekannt");
		addKontakt(adressbuch, "Niklas", "FVR", "Hamburg", "unbekannt", "unbekannt");
		addKontakt(adressbuch, "Test", "Test", "test", "test", "test");
	}
	//Datenbank Kontakte hinzufügen
	static void addDefaultDBEntries() {
		System.out.println("Standart Datensätze werden hinzugefügt");
		Database.insertMember("Daniel", "Becker", "Hamburg", "19.03.2002", 013456);
		Database.insertMember("Niklas", "FVR", "Hamburg", "unbekannt", 98765);
		Database.insertMember("Test", "Test", "test", "test", 1209);
	}
	
	private static void addKontakt(Adressbuch adressBuch, String vorname, String nachname, String wohnort, String geburtsdatum, String telefonnr) {
		Kontakt k = new Kontakt();
		k.setVorname(vorname);
		k.setNachname(nachname);
		k.setWohnort(wohnort);
		k.setGeburtsdatum(geburtsdatum);
		k.setTelefonnr(telefonnr);
		adressBuch.addKontakt(k);
	}
}
