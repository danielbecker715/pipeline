import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.*;

@SuppressWarnings("serial")
public class SaveMethod extends JFrame {
	public static void chooseMethod() {
		JFrame frame = new JFrame("Wie möchtest du dein Adressbuch speichern?");
	    JPanel panel = new JPanel();
	      
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setSize(850,400);
		JLabel label1 = new JLabel("Wie möchtest du dein Adressbuch speichern?");
		
		JButton button1 = new JButton("Datenbank");
		JButton button2 = new JButton("Lokale Datei");
		
		button1.setPreferredSize(new Dimension(300, 100));		// Button1 größe andern
		button2.setPreferredSize(new Dimension(300, 100));		// Button2 größe andern
        panel.setBackground(new java.awt.Color(255,255,255));	// Hintergrund zu Weiß ändern
        
		panel.add(label1);					// Label1 zum Pannel hinzufügen
		panel.add(button1);					// Button1 zum Pannel hinzufügen
		panel.add(button2);					// Button2 zum Pannel hinzufügen
	    frame.add(panel);					// Pannel zur App hinzufügen
	    frame.setLocationRelativeTo(null);  // Zentrieren
		frame.setVisible(true);
		
		button1.addActionListener(new ActionListener() { //Erkennung zum speichern in Datenbank
		    public void actionPerformed(ActionEvent e)
		    {
		    	frame.dispose(); //GUI entfernen
		    	saveToDatabase();
		    }
		});
		
		button2.addActionListener(new ActionListener() { //Erkennung zum speichern in Datei
		    public void actionPerformed(ActionEvent e)
		    {
		    	frame.dispose(); //GUI entfernen
		    	saveToFile();
		    }
		});
  }
	public static void saveToFile() {
		//Speichern in Datei
		System.out.println("Adressbuch wird gespeichert in einer Datei");
		
		String filename = "C:\\Files\\Adressbuch.txt";
		
		if (new File(filename).exists()) {
			// eine Datei mit Adressen einlesen:
			Adressbuch a2 = Adressbuch.readFromFile(filename);
			System.out.println("Adressbuch gelesen aus " + filename);
			a2.show();
		} else {
			// ein neues, leeres Adressbuch
			Adressbuch a1 = new Adressbuch();
			Main.addEntries(a1);
			a1.saveToFile(filename);
			a1.show();
			System.out.println("Adressbuch wurde gespeichert in " + filename);
		}
	}

	
	public static void saveToDatabase() {
		Adressbuch adressbuch = new Adressbuch();
		//Speichern in Datenbank
		System.out.println("Adressbuch wird gespeichert in Datenbank");
		
		//Prüfen ob es schon einträge gibt
		Integer result = Database.countMembers();
		if (result != null) {
			if (result > 0) {
				// Datenbankeinträge vorhanden
				System.out.println(result + " Datensäzte vorhanden");
				Kontakt kontakt = Database.selectMembers();

				if (kontakt != null) {
					adressbuch.addKontakt(kontakt);
				}
				adressbuch.show();

				//System.out.format("%s;%s;%s;%s;%s\n", firstName, lastName, Geburtsdatum, Wohnort, Telefonr);

			} else {
				// Keine Datenbankeinträge vorhanden
				System.out.println("Datenbank leer");
				Main.addDefaultDBEntries();
			}
		}
	}
}
