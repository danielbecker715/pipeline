import java.sql.*;

public class Database {

	public static Integer countMembers() {
		Connection con = null;
		try {
		    Class.forName("com.mysql.cj.jdbc.Driver");
		    // Initiating MySQL connection
		    con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Adressbuch?serverTimezone=MET", "root", "root");
		    Statement stmt = con.createStatement();
		    ResultSet rs = stmt.executeQuery("SELECT COUNT(*) FROM kontakte");
		    if (rs.first()) {
		    	return rs.getInt(1);
		    }
		    return null;
		} catch (Exception e) {
		    e.printStackTrace();
		    return null;
		} finally {
		    try {
		    	if (con != null) {
					con.close();
		    	}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static String insertMember(String vorname, String nachname, String wohnort, String geburtsdatum, Integer telefonnr) {
		Connection con = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			// Initiating MySQL connection
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Adressbuch?serverTimezone=MET", "root", "root");
			Statement stmt = con.createStatement();
			String query = "INSERT INTO `kontakte` (Vorname, Nachname, Geburtsdatum, Wohnort, Telefonnr) VALUES ('"+vorname+"', '"+nachname+"', '"+geburtsdatum+"', '"+wohnort+"', '"+telefonnr+"')";
			stmt.executeUpdate(query);
			System.out.println("Benutzer erstellt: " + vorname + " " + nachname);
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public static Kontakt selectMembers() {
		Connection con = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			// Initiating MySQL connection
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Adressbuch?serverTimezone=MET", "root", "root");
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM kontakte;");
			while (rs.next())
			{
				int id = rs.getInt("id");
				String firstName = rs.getString("Vorname");
				String lastName = rs.getString("Nachname");
				String Geburtsdatum = rs.getString("Geburtsdatum");
				String Wohnort = rs.getString("Wohnort");
				int Telefonr = rs.getInt("Telefonnr");

				return Kontakt.createFromRawString(firstName+";"+lastName+";"+Geburtsdatum+";"+Wohnort+";"+Telefonr);
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
