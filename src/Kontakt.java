public class Kontakt {
	
	private String vorname;
	private String nachname;
	private String geburtsdatum; // Idee: Später als Datumstyp!
	private String wohnort;
	private String telefonnr;
	
	public Kontakt() {
	}
	
	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getGeburtsdatum() {
		return geburtsdatum;
	}

	public void setGeburtsdatum(String geburtsdatum) {
		this.geburtsdatum = geburtsdatum;
	}

	public String getWohnort() {
		return wohnort;
	}

	public void setWohnort(String wohnort) {
		this.wohnort = wohnort;
	}

	public String getTelefonnr() {
		return telefonnr;
	}

	public void setTelefonnr(String telefonnr) {
		this.telefonnr = telefonnr;
	}
	
	public String getVorname() {
		return vorname;
	}
	
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	
	
	
	/**
	 * Erstellt einen neuen Kontakt, indem eine konkrete Zeile aus einer Kontakte-Datei eingelesen wird.
	 * @param zeileAusKontaktdatei String der Kontakt-Datei-Zeile
	 * @return der neue Kontakt oder null, falls der String ungültig ist.
	 */
	static Kontakt createFromRawString(final String zeileAusKontaktdatei) { // das ist eine "Factory-Methode" :-)
		Kontakt ret = new Kontakt();
		String[] kontaktdaten = zeileAusKontaktdatei.split(";");
		try {
			ret.vorname = kontaktdaten[0];
			ret.nachname = kontaktdaten[1];
			ret.geburtsdatum = kontaktdaten[2];
			ret.wohnort = kontaktdaten[3];
			ret.telefonnr = kontaktdaten[4];
		} catch (ArrayIndexOutOfBoundsException e) {
			ret = null;
			System.err.println("Ungültiger Datensatz: " + zeileAusKontaktdatei);
		}
		
		return ret;
	}
	

	// z.B.: Daniel;Becker;19.3.2002;Hamburg;egal  // serialize
	public String toRawString() {
		return    this.vorname + ";"
				+ this.nachname + ";"
				+ this.geburtsdatum + ";"
				+ this.wohnort + ";"
				+ this.telefonnr;
	}
	
	public void show() {
		System.out.println("Kontakt: " + vorname + " " + nachname);
		System.out.println("Adresse: " + wohnort);
		System.out.println("Geburtstag: " + geburtsdatum);
		System.out.println("TelefonNr: " + telefonnr);
	}

}
